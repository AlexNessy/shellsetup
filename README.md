# This project is for setting up shell and all customizations from a single script
## CURRENT ADDITIONS:
[] Installing packages  
[] Setting up aliases  
[] ohmyzsh setup  
[] ohmybash setup  

## TO DO:  
[] Package manager chooser  
[*] Merge fullsetup and litesetup into install.sh  
[] Splitting into multiple scripts (once install.sh gets too full)  